import { Component } from '@angular/core';

import { Platform, MenuController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  firstInstall: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private toastCtrl: ToastController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      // this.splashScreen.hide();
      this.statusBar.show();
    });
    this.platform.backButton.subscribe((route) => {
      console.log(route);
      if (this.router.url === '' || this.router.url === 'tabs/home' || this.router.url === 'tabs/video' || this.router.url === 'tabs/category' || this.router.url === 'tabs/profile') {
        // if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
        // } else {
        //   this.toastCtrl.create({
        //     message: 'Press back again to exit App',
        //     duration: 3000,
        //     position: 'bottom'
        //   });
        //   this.lastTimeBackPress = new Date().getTime();
        // }
        navigator['app'].exitApp();
      }
    });
  }
}
