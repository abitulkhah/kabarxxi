import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  categories = [
    {
      name: 'Nasional',
      image: '/assets/berita1.jpeg'
    },
    {
      name: 'Daerah',
      image: '/assets/berita1.jpeg'
    },
    {
      name: 'Megapolitan',
      image: '/assets/berita1.jpeg'
    },
    {
      name: 'Hukrim',
      image: '/assets/berita1.jpeg'
    },
    {
      name: 'Peristiwa',
      image: '/assets/berita1.jpeg'
    },
    {
      name: 'Pendidikan',
      image: '/assets/berita1.jpeg'
    },
    {
      name: 'Ekonomi',
      image: '/assets/berita1.jpeg'
    },
    {
      name: 'Politik',
      image: '/assets/berita1.jpeg'
    },
    {
      name: 'Info dan Tips',
      image: '/assets/berita1.jpeg'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
