import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  isLogin: boolean;

  constructor(private navCtrl: NavController) {

  }

  ionViewDidEnter() {
    this.isLogin = localStorage.getItem('isLogin') == 'true' ? true : false;
  }

  ngOnInit() {

  }

  settingPage() {
    this.navCtrl.navigateForward('setting');
  }

  notifPage() {
    this.navCtrl.navigateForward('notification');
  }

  bookmarkPage() {
    this.navCtrl.navigateForward('bookmark');
  }

  loginPage() {
    this.navCtrl.navigateForward('login');
  }

  signUpPage() {
    this.navCtrl.navigateForward('signup');
  }
}
