import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSearchPage } from './detail-search.page';

describe('DetailSearchPage', () => {
  let component: DetailSearchPage;
  let fixture: ComponentFixture<DetailSearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
