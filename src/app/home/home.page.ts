import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  public category: string = 'newest';
  public categories = ['newest', 'mainwNews', 'mostPopular', 'mostCommented', 'lifeStyle']

  onTabChanged(tabName) {
    this.category = tabName;
  }

  constructor(private navCtrl: NavController) {

  }

  ngOnInit(): void {

  }

  detailNews() {
    this.navCtrl.navigateForward('detail-news');
  }

  detailSearch() {
    this.navCtrl.navigateForward('detail-search');
  }

  changePage(event, page) {
    console.log(event);
    this.navCtrl.navigateForward('main-news');
  }



}
