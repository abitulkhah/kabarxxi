import { Component, OnInit } from '@angular/core';
import { ScrollDetail } from '@ionic/core';
import { NavController } from '@ionic/angular';
// import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-detail-news',
  templateUrl: './detail-news.page.html',
  styleUrls: ['./detail-news.page.scss'],
})
export class DetailNewsPage implements OnInit {

  showToolbar = false;
  newsTag = ["Berita 1", "Berita 2", "Berita 3", "Berita 4", "Berita 5"];
  constructor(private navCtrl: NavController, private actionSheetController: ActionSheetController) { }

  ngOnInit() {

  }

  fontSetting() {

  }

  shareNews() {
    // this.socialSharing.share("Kabar XXI", "Kabar XXI Share News", "http://www.kabarxxi.com");
  }

  bookmarkNews() {

  }

  async reactionNews() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Pilih Reaction',
      buttons: [{
        text: 'Senang',
        icon: 'happy',
        cssClass: 'happy-emoticon',
        handler: () => {
          console.log('Senang clicked');
        }
      }, {
        text: 'Sedih',
        icon: 'sad',
        handler: () => {
          console.log('Sedih clicked');
        }
      }, {
        text: 'Biasa',
        icon: 'heart-empty',
        handler: () => {
          console.log('Biasa clicked');
        }
      }, {
        text: 'Cuek',
        icon: 'heart-half',
        handler: () => {
          console.log('Cuek clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  commentNews() {
    this.navCtrl.navigateForward('comment');
  }

}
